USE aptunes_examen;

SELECT Albums.Titel AS 'Album', Bands.Naam AS 'Band'
FROM Albumreleases
INNER JOIN Albums ON Albumreleases.Albums_Id = Albums.Id
INNER JOIN Bands ON Albumreleases.Bands_Id = Bands.Id;