USE aptunes_examen;
DROP PROCEDURE IF EXISTS GetSongDuration;

DELIMITER $$
USE `aptunes_examen`$$
CREATE PROCEDURE `GetSongDuration`(IN albumId INT)
BEGIN

    DECLARE ok INTEGER DEFAULT 0;
    DECLARE songDuration TINYINT UNSIGNED DEFAULT 0;
    DECLARE albumDuration INTEGER DEFAULT 0;

    DECLARE currentAlbum
        CURSOR FOR SELECT Lengte FROM Liedjes WHERE Albums_Id = albumId;

    DECLARE CONTINUE HANDLER
        FOR NOT FOUND SET ok = 1;

    OPEN currentAlbum;

    getSongDuration:
    LOOP
        FETCH currentAlbum INTO songDuration;
        IF ok = 1
        THEN
            LEAVE getSongDuration;
        END IF;
        SET albumDuration = albumDuration + songDuration;
    END LOOP getSongDuration;

    CLOSE currentAlbum;

    IF albumDuration > 60 THEN
        SELECT 'Lange duurtijd' AS 'Duurtijd';
    ELSE
        SELECT 'Normale duurtijd' AS 'Duurtijd';
    END IF;


END$$

DELIMITER ;