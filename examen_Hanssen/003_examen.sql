USE aptunes_examen;
DROP PROCEDURE IF EXISTS PopulateLiedjesGenres;

DELIMITER $$
USE `aptunes_examen`$$
CREATE PROCEDURE `PopulateLiedjesGenres`()
BEGIN

    DECLARE numberOfLiedjes INT DEFAULT 0;
    DECLARE numberOfGenres INT DEFAULT 0;
    DECLARE randomLiedjesId INT DEFAULT 0;
    DECLARE randomGenreId INT DEFAULT 0;
    DECLARE combinationExist TINYINT DEFAULT 0;

    SELECT COUNT(*)
    INTO numberOfLiedjes
    FROM Liedjes;
    SELECT COUNT(*)
    INTO numberOfGenres
    FROM Genres;

    SET randomLiedjesId = FLOOR(RAND() * (numberOfLiedjes) + 1);
    SET randomGenreId = FLOOR(RAND() * (numberOfGenres) + 1);

    SELECT EXISTS(
                   SELECT *
                   FROM Liedjesgenres
                   WHERE Liedjes_Id = randomLiedjesId
                     AND Genres_Id = randomGenreId
               )
    INTO combinationExist;


    IF combinationExist = 0 THEN
        INSERT INTO Liedjesgenres(Genres_Id, Liedjes_Id)
            VALUE (
                   randomGenreId, randomLiedjesId
            );
    END IF;

END$$

DELIMITER ;