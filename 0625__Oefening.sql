DROP INDEX VoornaamAchternaamIdx ON Muzikanten;
CREATE INDEX AchternaamVoornaamIdx ON Muzikanten(Familienaam(9), Voornaam(9));
select Voornaam, Familienaam, count(Lidmaatschappen.Muzikanten_Id)
from Muzikanten inner join Lidmaatschappen
on Lidmaatschappen.Muzikanten_Id = Muzikanten.Id
group by Familienaam, Voornaam
order by Voornaam, Familienaam;