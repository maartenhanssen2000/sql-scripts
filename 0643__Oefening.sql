USE aptunes;
DROP procedure IF EXISTS CreateAndReleaseAlbum;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CreateAndReleaseAlbum` (IN titel VARCHAR(100), IN bands_Id INT)
BEGIN
    START TRANSACTION;
    INSERT INTO Albums(Titel) VALUE (titel);
    INSERT INTO Albumreleases(Bands_Id, Albums_Id) VALUE (bands_Id, LAST_INSERT_ID());
    COMMIT;
END $$

DELIMITER ;