
create
    definer = root@`%` procedure GetMuzikanten()
BEGIN
    SELECT
        Voornaam,
        Familienaam
    FROM
        Muzikanten
    ORDER BY 1,2; -- deze ; betekent niet dat de instructie mag uitgevoerd worden! ze is deel van de procedure
END;

