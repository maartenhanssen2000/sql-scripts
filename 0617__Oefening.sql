USE ModernWays;

DROP VIEW IF EXISTS AuteursBoekenRatings;
CREATE VIEW AuteursBoekenRatings
AS
SELECT Auteur, Titel, Rating
FROM AuteursBoeken
INNER JOIN GemiddeldeRatings;