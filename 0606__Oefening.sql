USE ModernWays;

SELECT Leden.Voornaam, Boeken.Titel
FROM Uitleningen
INNER JOIN Leden ON Uitleningen.Persoon_ID = Leden.Id
INNER JOIN Boeken ON Uitleningen.Boek_ID = Boeken.Id;