USE ModernWays;

SELECT DISTINCT Voornaam
FROM Studenten
WHERE Voornaam = ANY
(SELECT Voornaam FROM Personeelsleden WHERE Personeelsleden.Voornaam = ANY
(SELECT Voornaam FROM Directieleden));