USE aptunes;
DROP PROCEDURE IF EXISTS GetAlbumDuration2;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration2`(IN album_id INT, OUT totalLength SMALLINT)
SQL SECURITY INVOKER 
BEGIN

    DECLARE ok INTEGER DEFAULT 0;
    DECLARE songLength TINYINT UNSIGNED;

    DECLARE currentSong
        CURSOR FOR SELECT Lengte FROM Liedjes WHERE Albums_Id = album_id;


    DECLARE CONTINUE HANDLER
        FOR NOT FOUND SET ok = 1;

    OPEN currentSong;

    getLength: LOOP
            FETCH currentSong INTO songLength;
            IF ok = 1
                THEN
                LEAVE getLength;
            END IF;
            SET totalLength = totalLength + songLength;
    END LOOP;

    CLOSE currentSong;
END$$

DELIMITER ;