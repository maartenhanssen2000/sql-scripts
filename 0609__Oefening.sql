USE ModernWays;

SELECT Games.Titel, Platformen.Naam
FROM Games
LEFT JOIN Releases on Games.Id = Releases.Games_Id
LEFT JOIN Platformen on Platformen.Id = Releases.Platformen_Id