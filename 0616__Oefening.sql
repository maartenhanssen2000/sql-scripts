USE ModernWays;

DROP VIEW IF EXISTS GemiddeldeRatings;
CREATE VIEW GemiddeldeRatings
AS
SELECT Boeken_Id,AVG(Rating) as "Rating"
FROM Reviews
GROUP BY Boeken_Id;