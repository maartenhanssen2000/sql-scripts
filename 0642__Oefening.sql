USE aptunes;
DROP procedure IF EXISTS CleanupOldMembership;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CleanupOldMemberships` (IN someDate DATE, OUT numberCleaned INT)
BEGIN
    start transaction;
    SELECT COUNT(*)
    INTO numberCleaned
    FROM Lidmaatschappen
    WHERE someDate > Einddatum;
    set sql_safe_updates = 0;
    DELETE FROM Lidmaatschappen
        WHERE someDate > Einddatum;
    set sql_safe_updates = 1;
    commit;
END $$

DELIMITER ;