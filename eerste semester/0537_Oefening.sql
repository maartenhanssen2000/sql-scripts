USE ModernWays;

SELECT Voornaam, Familienaam, COUNT(Titel) AS "aantal boeken"
FROM Boeken
GROUP BY Voornaam, Familienaam;