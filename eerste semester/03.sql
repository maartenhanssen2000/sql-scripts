USE TussentijdseEvaluatie;

CREATE TABLE Personen(
Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL COLLATE utf8mb4_0900_as_cs,
Familienaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL COLLATE utf8mb4_0900_as_cs,
Geboortedatum DATE NOT NULL,
GSMnummer FLOAT(10,0) NOT NULL
);

nummer moet char zijn