USE TussentijdseEvaluatie;

SET SQL_SAFE_UPDATES = 0;
UPDATE Vogels 
SET 
    Klank = 'cawcaw'
WHERE
    Klank = 'caw';
UPDATE Vogels 
SET 
    Klank = 'roekoeroekoe'
WHERE
    Klank = 'roekoe';
SET SQL_SAFE_UPDATES = 1;
