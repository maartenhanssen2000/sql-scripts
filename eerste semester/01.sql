USE TussentijdseEvalutie;

ALTER TABLE Recepten ADD COLUMN Opmerkingen VARCHAR(500) CHAR SET utf8mb4;
