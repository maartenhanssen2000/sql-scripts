USE modernways;

ALTER TABLE boeken
ADD COLUMN Personen_Id INT,
ADD CONSTRAINT FK_Boeken_Personen
    FOREIGN KEY (Personen_Id)
    REFERENCES personen(Id);