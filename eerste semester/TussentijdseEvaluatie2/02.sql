USE TussentijdseEvaluatie2;

SELECT Regisseur
FROM Films
GROUP BY Regisseur
HAVING AVG(STERREN) >= 3;