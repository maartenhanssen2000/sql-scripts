USE TussentijdseEvaluatie2;

CREATE TABLE Recensies(
    Naam VARCHAR(100) CHAR SET utf8mb4 COLLATE utf8mb4_0900_as_cs NOT NULL,
    Link VARCHAR(2083) CHAR SET utf8mb4 COLLATE utf8mb4_0900_as_cs NOT NULL,
    Cijfer TINYINT UNSIGNED NOT NULL,
    Film_Id INT,
    CONSTRAINT fk_Recensies_Films FOREIGN KEY (Film_Id)
    REFERENCES Films(Id)
);
