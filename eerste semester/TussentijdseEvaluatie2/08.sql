USE TussentijdseEValuatie2;

SELECT Regisseur, Genre, AVG(Sterren) AS "Gemiddeld aantal sterren"
FROM Films
Group BY Regisseur, Genre, Jaar
HAVING AVG(Sterren) >= 4 AND Jaar >= 2000;