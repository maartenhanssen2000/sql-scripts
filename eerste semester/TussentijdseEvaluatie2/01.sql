USE TussentijdseEvaluatie2;

SELECT Regisseur, COUNT(*) AS "Aantal films"
FROM films
Group By Regisseur;