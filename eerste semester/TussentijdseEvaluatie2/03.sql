USE TussentijdseEvaluatie2;

SELECT Jaar, Genre, Inkomsten
FROM films
Group By Jaar, Genre
Having Max(Inkomsten) < 200000000;