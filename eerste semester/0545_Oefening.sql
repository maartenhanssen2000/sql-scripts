USE ModernWays;

SELECT Artiest, SUM(Aantalbeluisteringen) AS 'Totaal aantal Beluisteringen'
FROM liedjes
WHERE CHAR_LENGTH(Artiest) > 10
GROUP BY Artiest
HAVING SUM(Aantalbeluisteringen) > 100;