DROP DATABASE IF EXISTS Examen;
CREATE DATABASE Examen;
USE Examen;

-- maak hier de tabel Merken aan zodat volgende instructies zullen werken
-- een merknaam bevat maximum 100 karakters en is verplicht

CREATE TABLE Merken(
    Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL
);

INSERT INTO Merken (Naam)
VALUES
('IBM'),
('HP'),
('Dell'),
('Bull'),
('Sun'),
('Acer');

-- maak hier de tabel Categorieen aan zodat volgende instructies zullen werken
-- een omschrijving bevat maximum 100 karakters en is verplicht

CREATE TABLE Categorieen(
    Omschrijving VARCHAR(100) CHAR SET utf8mb4 NOT NULL
);

INSERT INTO Categorieen (Omschrijving)
VALUES
('pc'),
('printer'),
('server');

-- maak hier de tabel Processoren aan zodat volgende instructies zullen werken
-- een omschrijving bevat maximum 100 karakters en is verplicht

CREATE Table Processoren(
    Omschrijving VARCHAR(100) CHAR SET utf8mb4 NOT NULL
);

INSERT INTO Processoren (Omschrijving)
VALUES
('3 Ghz'),
('300 Mhz'),
('2,6 Ghz');

-- maak hier de tabel Subcategorieën aan zodat volgende instructies zullen werken
-- een omschrijving bevat maximum 100 karakters en is verplicht
-- een subcategorie verwijst verplicht naar een categorie

CREATE TABLE Subcategorieen(
    Omschrijving VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
    Categorieen_Id INT NOT NULL
);

INSERT INTO Subcategorieen (Omschrijving,Categorieen_Id)
VALUES
('desktop',1),
('laptop',1),
('inkjet',2),
('laser',2),
('webserver',3),
('databaseserver',3),
('mailserver',3);

-- maak hier de tabel Producten aan zodat volgende instructies zullen werken
-- een naam bevat maximum 100 karakters en is verplicht
-- de drie verwijzingen (herkenbaar aan de naam) zijn verplicht
-- het gewicht bevat maximum 15 karakters en is verplicht
-- het is verplicht bij te houden of een product in voorraad is of niet

CREATE Table Producten(
    Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
    Processoren_Id INT NOT NULL,
    Subcategorieen_Id INT NOT NULL,
    Gewicht VARCHAR(15) CHAR SET utf8mb4 NOT NULL,
    Merken_Id INT NOT NULL,
    InVoorraad BOOLEAN NOT NULL
);

INSERT INTO Producten (Naam, Processoren_Id, Subcategorieen_Id, Gewicht, Merken_Id, InVoorraad)
VALUES
('FireX4500',3,6,'77 kg',5,TRUE),
('Color Laserjet 1600',2,4,'20 kg',2,TRUE),
('XPS M1210',1,2,'10 kg',3,TRUE),
('Netra 1280',1,6,'132 kg',5,FALSE),
('946 (all in one)',2,3,'12 kg',3,FALSE),
('6217 Intellistation A Pro',1,1,'22 kg',1,TRUE),
('Escale PL250',3,6,'50 kg',4,FALSE),
('Veriton 6800',1,1,'12 kg',6,TRUE),
('Aspire L100250',1,1,'9 kg',6,TRUE),
('Laserjet P2015d',2,4,'19 kg',2,FALSE);