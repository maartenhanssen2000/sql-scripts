USE Examen;


INSERT INTO Processoren(
Omschrijving
)
VALUES(
'2,6 GHz'
);

INSERT INTO Subcategorieen(
Omschrijving
)
VALUES (
'Desktop'
);

INSERT INTO Merken(
Naam
)
VALUES(
'Dell'
);

INSERT INTO Producten(
processoren_Id
)
SELECT ID
FROM Processoren
WHERE Omschrijving = '2,6 GHz';

INSERT INTO Producten(
Subcategorieen_id
)
SELECT ID
FROM Subcategorieen
WHERE Omschrijving = 'Desktop';

INSERT INTO Producten(
Merken_Id
)
SELECT ID
FROM Merken
WHERE Naam = 'Dell';

SET SQL_SAFE_UPDATES = 0;
UPDATE Producten
INNER JOIN Processoren ON Producten.Processoren_Id = Processoren.Id
INNER JOIN Subcategorieen ON Producten.Subcategorieen_Id = Subcategorieen.Id
INNER JOIN Merken ON Producten.Merken_Id = Merken.Id
SET Naam = 'XPS 710', Gewicht = '14 kg', InVoorraad = 1
WHERE Processoren.Omschrijving = '2,6 GHz' AND Subcategorieen.Omschrijving = 'Desktop' AND Merken.Naam = 'Dell';


