USE Examen;



SELECT Subcategorieen.Omschrijving AS "Omschrijving subcategorie",
Naam AS "Naam Product" 
FROM Producten
INNER JOIN Subcategorieen ON Producten.Subcategorieen_Id = Subcategorieen.Id
INNER JOIN Processoren ON Producten.Processoren_Id = Processoren.Id
WHERE Processoren.Omschrijving = "300 MHz";