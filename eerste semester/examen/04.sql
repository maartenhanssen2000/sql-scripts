USE Examen;

SET SQL_SAFE_UPDATES = 0;
UPDATE Producten 
INNER JOIN Processoren ON Producten.Processoren_Id = Processoren.Id
SET Processoren.Omschrijving = "3 GHz"
WHERE Processoren_Id = 1;
SET SQL_SAFE_UPDATES = 1;