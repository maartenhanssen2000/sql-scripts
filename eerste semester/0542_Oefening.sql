USE ModernWays;

SELECT Artiest, SUM(Aantalbeluisteringen) AS 'Aantal beluisteringen'
FROM liedjes
GROUP BY Artiest
HAVING Sum(Aantalbeluisteringen) > 100;