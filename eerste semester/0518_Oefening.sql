USE ModernWays;

ALTER TABLE Huisdieren ADD COLUMN Geluid VARCHAR (20) CHAR SET utf8mb4;
SET SQL_SAFE_UPDATES = 0;
UPDATE Huisdieren SET Geluid = 'woef' WHERE soort = 'Hond';
UPDATE Huisdieren SET Geluid = 'maw' WHERE soort = 'Kat';
SET SQL_SAFE_UPDATES = 1;

