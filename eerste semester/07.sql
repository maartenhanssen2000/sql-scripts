USE TussentijdseEvaluatie;

SET SQL_SAFE_UPDATES = 0;
ALTER TABLE Vogels CHANGE Soort Soort VARCHAR(100) CHAR SET utf8mb4 NOT NULL;
DELETE FROM Vogels 
WHERE
    Soort LIKE 'a%a';
SET SQL_SAFE_UPDATES = 1;

alter table weg en colate