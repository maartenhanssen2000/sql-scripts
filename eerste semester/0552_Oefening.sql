USE modernways;

ALTER TABLE baasje ADD COLUMN HuisdierenId INT;
 
ALTER TABLE baasje ADD CONSTRAINT FK_Huisdieren FOREIGN KEY (HuisdierenId) REFERENCES huisdieren(Id);