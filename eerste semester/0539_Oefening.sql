USE ModernWays;

SELECT AVG(Leeftijd) AS 'Gemiddelde leeftijd', Max(Leeftijd) AS 'Hoogste leeftijd', COUNT(*) AS 'Totaal aantal'
FROM huisdieren