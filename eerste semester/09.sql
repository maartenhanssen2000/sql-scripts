USE TussentijdseEvaluatie;

SET SQL_SAFE_UPDATES = 0;
DELETE FROM Planten WHERE Categorie LIKE '%plant' AND GrondType = 'Mineraalrijke grond';
SET SQL_SAFE_UPDATES = 1;