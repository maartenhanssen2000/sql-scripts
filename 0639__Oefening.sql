use ModernWays;

insert into Personen (
   Voornaam, Familienaam
)
values (
   'Jean-Paul', 'Sartre'
);

insert into Boeken (
    Titel,
    Verschijningsdatum,
    Uitgeverij,
    Personen_Id
)
values (
   'De woorden',
   '1961',
    'de Bezige Bij',
   (select Id from Personen where
       Familienaam = 'Sartre' and Voornaam = 'Jean-Paul'))