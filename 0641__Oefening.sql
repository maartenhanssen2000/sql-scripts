USE aptunes;
DROP procedure IF EXISTS NumberOfGenres;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `NumberOfGenres` (OUT Total TINYINT)
BEGIN
    SELECT COUNT(*)
    INTO Total
    FROM Genres;
END $$

DELIMITER ;