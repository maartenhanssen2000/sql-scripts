USE TussentijdseEvaluatie1BDB;

CREATE VIEW BoekenInfo
AS
SELECT Titel, CONCAT(Auteurs.Voornaam,' ',Auteurs.Familienaam) AS 'naam auteur',Uitgeverijen.Naam AS 'naam uitgeverij'
FROM  Boeken
INNER JOIN Auteurs ON Boeken.Auteurs_Id = Auteurs.Id
INNER JOIN Uitgeverijen ON Boeken.Uitgeverijen_Id = Uitgeverijen.Id