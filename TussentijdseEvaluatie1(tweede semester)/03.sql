USE TussentijdseEvaluatie1BDB;

SELECT Titel
FROM Boeken
INNER JOIN Uitgeverijen ON Boeken.Uitgeverijen_Id = Uitgeverijen.Id
WHERE Uitgeverijen.Naam = "Papyrus";