USE TussentijdseEvaluatie1BDB;

CREATE VIEW OnpopulaireBoeken
AS
SELECT Boeken.Titel
FROM Ontleningen
RIGHT JOIN Boeken ON Ontleningen.Boeken_Id = Boeken.Id
WHERE Ontleningen.Boeken_Id IS NULL