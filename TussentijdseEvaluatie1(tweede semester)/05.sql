USE TussentijdseEvaluatie1BDB;

SELECT Boeken.Titel
FROM Ontleningen
INNER JOIN Boeken ON Ontleningen.Boeken_Id = Boeken.Id
GROUP BY Boeken.Titel
HAVING COUNT(Leden_Id) > 2;