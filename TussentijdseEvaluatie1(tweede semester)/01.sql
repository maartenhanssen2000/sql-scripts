USE TussentijdseEvaluatie1BDB;

SELECT Boeken.Titel, concat(Leden.Voornaam , " ", Leden.Familienaam) AS "naam"
FROM Ontleningen
INNER JOIN Boeken ON Ontleningen.Boeken_Id = Boeken.Id
INNER JOIN Leden ON Ontleningen.Leden_Id = Leden.Id;