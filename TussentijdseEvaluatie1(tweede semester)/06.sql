USE TussentijdseEvaluatie1BDB;

SELECT CONCAT(Leden.Voornaam,' ',Leden.Familienaam) AS 'Volledige Naam', IF(Boeken.Id IS NOT NULL, Boeken.Titel, 'Heeft nog nooit iets uitgeleend') AS 'Titel'
FROM Ontleningen
LEFT JOIN Leden ON Ontleningen.Leden_Id = Leden.Id
LEFT JOIN Boeken on Ontleningen.Boeken_Id = Boeken.Id
WHERE Boeken_Id IS NULL

UNION ALL

Select IF(Leden.Id IS NOT NULL,Leden.Voornaam, 'Is nog nooit door iemand uitgeleend'), Boeken.Titel
FROM Ontleningen
RIGHT JOIN Leden ON Ontleningen.Leden_Id = Leden.Id
RIGHT JOIN Boeken ON Ontleningen.Boeken_Id = Boeken.Id
WHERE Leden_Id IS NULL