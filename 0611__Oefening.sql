USE ModernWays;

SELECT Games.Titel, IF(Platformen.Id IS NOT NULL, Platformen.Naam, 'Geen Platformen') AS Naam
FROM Games
LEFT JOIN Releases on Games.Id = Releases.Games_Id
LEFT JOIN Platformen on Platformen.Id = Releases.Platformen_Id
WHERE Platformen.Id IS NULL

UNION ALL
Select IF(Games.Id IS NOT NULL,Games.Titel, 'Geen Games'), Platformen.Naam
FROM Games
RIGHT JOIN Releases on Games.Id = Releases.Games_Id
RIGHT JOIN Platformen on Platformen.Id = Releases.Platformen_Id
WHERE Games.Id IS NULL