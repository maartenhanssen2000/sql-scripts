-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
use ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- gegevens uit de tabel Boeken overzetten naar de tabel Personen
-- we gebruiken hiervoor een subquery
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;

-- primary key toevoegen en meedere gegevens aan de Personen tabel
-- AanspreekTitel, Adress(straat, Hnr, stad), Commentaar en Biografie
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null
);

-- foreign key en gegevens toevoegen bij boeken
-- link leggen tussen boeken en personen
alter table Boeken add Personen_Id int null;
update Boeken cross join Personen
set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
      Boeken.Familienaam = Personen.Familienaam;
alter table Boeken change Personen_Id Personen_Id int not null;

-- dobbele kolommen verwijderen uit de tabel boeken
alter table Boeken drop column Voornaam, drop column Familienaam;

-- constrain toevoegen aan foreign key
alter table Boeken add constraint fk_Boeken_Perosonen
foreign key(Personen_Id) references Personen(Id);