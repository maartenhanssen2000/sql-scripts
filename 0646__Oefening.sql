USE aptunes;
DROP procedure IF EXISTS MockAlbumReleases;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleases` (IN extraReleases INT)
BEGIN

-- variable declareren
    DECLARE counter INT DEFAULT 0;

REPEAT
    CALL MockAlbumReleaseWithSuccess(@succes);
    IF @succes = 1 THEN
        SET counter = counter + 1;
    END IF;
UNTIL counter = extraReleases
END REPEAT;

END $$

DELIMITER ;