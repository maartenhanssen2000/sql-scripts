USE ModernWays;

SELECT Taken.Omschrijving, Leden.Voornaam
FROM Taken
LEFT JOIN Leden ON Taken.Leden_Id = Leden.Id;